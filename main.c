#include <avr/io.h>
#include <util/delay.h>
int main(void){
  DDRB = 0b11111110;
  DDRD = 0xFF;
  PORTD = 0x00;
  PORTB = 0x00;
  //mandatory_part();
  additional_part();
  return 0;
}
int mandatory_part(void){
  while(1)
  {
    if(!(PINB&(1<<0))){//when the button is pressed the red led shine
    	PORTB = PORTB|(1<<1);
    }
    else
    {
      PORTB = PORTB&0xFD;
    }
    PORTB = PORTB|(1<<2);//make the 1st led shine without changing the other
    _delay_ms(250);
    if(!(PINB&(1<<0))){
    	PORTB = PORTB|(1<<1);
    }
    else
    {
      PORTB = PORTB&0xFD;
    }
    PORTB = PORTB|(1<<3);//make the 2nd led shine without changing the other
    _delay_ms(250);
    if(!(PINB&(1<<0))){
    	PORTB = PORTB|(1<<1);
    }
    else
    {
      PORTB = PORTB&0xFD;
    }
    PORTB = PORTB|(1<<4);//make the 3rd led shine without changing the other
    _delay_ms(250);
    if(!(PINB&(1<<0))){
    	PORTB = PORTB|(1<<1);
    }
    else
    {
      PORTB = PORTB&0xFD;
    }
    PORTB = PORTB|(1<<5);//make the 4th led shine without changing the other
    _delay_ms(250);
    if(!(PINB&(1<<0))){
    	PORTB = PORTB|(1<<1);
    }
    else
    {
      PORTB = PORTB&0xFD;
    }
    PORTB = PORTB&0b11000011;// reset all the led
  }
}
int additional_part(void){
  char count = 1;
  char way = 1;
  char i = 0; 
  while(1){
    if(i==0){
    	PORTD = 0b11111101;//show a 0 on the screen
    }
    else{ 
      if (i==1){
    	PORTD = 0b01100000;//show a 1 on the screen
      }
        else {
          if (i==2){
      	PORTD = 0b11011010;//show a 2 on the screen
      	}
          else {
            if (i==3){
        PORTD = 0b11110010;//show a 3 on the screen
      }
            else {
              if (i==4){
      	PORTD = 0b01100110;//show a 4 on the screen
      }
              else {
                if (i==5){
      	PORTD = 0b10110110;//show a 5 on the screen
      }
                else {
                  if (i ==6){
      	PORTD = 0b10111110;//show a 6 on the screen
      }
                  else {
                    if (i == 7){
        PORTD = 0b11100000;//show a 7 on the screen
      }
                    else {
                      if ( i == 8 ){
      	PORTD = 0b11111110;//show a 8 on the screen
      }
      else {
        PORTD = 0b11110110;//show a 9 on the screen
      }
                    }
                  }
                }
              }
            }
          }
        }
    }
    if((PINB&(1<<0))){
    	way *= -1;
    }
    count = (count+way);
    if(count == -1)
      count = 3;
    if(count == 4 )
      count = 0;
    if(count == 0){
    	PORTB = (1<<PB2)|(1<<PB1);
    }
    else{
      if(count == 1){
      	PORTB = (1<<PB3)|(1<<PB1);
      }
      else{
        if(count == 2){
        	PORTB = (1<<PB4)|(1<<PB1);
        }
        else{
        	PORTB = (1<<PB5)|(1<<PB1);
        }
      }
    }
    i = (i+1)%10;
    _delay_ms(250);
  }
}
